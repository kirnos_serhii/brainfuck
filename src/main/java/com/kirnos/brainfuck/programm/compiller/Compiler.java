package com.kirnos.brainfuck.programm.compiller;

import com.kirnos.brainfuck.exception.BrainfuckRuntimeException;
import com.kirnos.brainfuck.exception.CompileProgramException;
import com.kirnos.brainfuck.programm.Program;
import com.kirnos.brainfuck.programm.command.Command;
import com.kirnos.brainfuck.programm.command.impl.WhileCommand;
import com.kirnos.brainfuck.programm.compiller.supplier.SupplierCommandContainer;

import java.util.ArrayList;
import java.util.List;

import static com.kirnos.brainfuck.util.Constants.ALL_COMMANDS;
import static com.kirnos.brainfuck.util.Constants.CLOSE_BRACKET;
import static com.kirnos.brainfuck.util.Constants.OPEN_BRACKET;

/**
 * Class for compiling string to program code entity {@link com.kirnos.brainfuck.programm.Program}
 *
 * @author Serhii Kirnos
 */
public class Compiler {

    /**
     * Validate {@code programCode} string and compiles program. Builds
     * {@link com.kirnos.brainfuck.programm.Program} entity based on {@code programCode} string
     *
     * @param programCode program code
     * @return compiled program
     * @throws CompileProgramException throws exception if the code {@code programCode} is invalid
     */
    public Program compile(String programCode) throws CompileProgramException {
        if (!validateBrackets(programCode) || !validateSymbols(programCode)) {
            throw new CompileProgramException("Incorrect program code.");
        }

        String[] symbolsArray = programCode.split("");
        List<Command> commandList = getCommands(symbolsArray, 0, symbolsArray.length - 1);

        return new Program(commandList);
    }

    private List<Command> getCommands(String[] symbolsArray, Integer startCommandPointer, Integer endCommandPointer) {
        List<Command> commandList = new ArrayList<>();
        Integer currentCommandPointer = startCommandPointer;
        while (currentCommandPointer <= endCommandPointer){
            if (!symbolsArray[currentCommandPointer].equals(OPEN_BRACKET)) {
                commandList.add(SupplierCommandContainer.get(symbolsArray[currentCommandPointer]).get());
                currentCommandPointer++;
            } else {
                int endSubCommandPointer = findEndCycle(symbolsArray, currentCommandPointer + 1, endCommandPointer);
                Command whileCommand = new WhileCommand(
                        getCommands(symbolsArray, currentCommandPointer + 1, endSubCommandPointer - 1));
                commandList.add(whileCommand);
                currentCommandPointer = endSubCommandPointer + 1;
            }
        }
        return commandList;
    }

    private Integer findEndCycle(String[] symbolsArray, Integer intervalStart, Integer intervalEnd) {
        Integer currentCommandPointer = intervalEnd;
        while (currentCommandPointer >= intervalStart) {
            if (symbolsArray[currentCommandPointer].equals(CLOSE_BRACKET)) {
                return currentCommandPointer;
            }
            currentCommandPointer--;
        }
        throw new BrainfuckRuntimeException("Incorrect program code");
    }

    private boolean validateBrackets(String programCode) {
        String[] symbolsArray = programCode.split("");
        int numberOpenBrackets = 0;
        for (String commandSymbol : symbolsArray) {
            if (OPEN_BRACKET.equals(commandSymbol)) {
                numberOpenBrackets++;
            } else if (CLOSE_BRACKET.equals(commandSymbol)) {
                numberOpenBrackets--;
            }
            if (numberOpenBrackets < 0) {
                return false;
            }
        }
        return numberOpenBrackets == 0;
    }

    private boolean validateSymbols(String programCode) {
        String[] symbolsArray = programCode.split("");
        for (String commandSymbol : symbolsArray) {
            if (!ALL_COMMANDS.contains(commandSymbol)) {
                return false;
            }
        }
        return true;
    }
}
