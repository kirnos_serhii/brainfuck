package com.kirnos.brainfuck.programm.compiller.supplier;

import com.kirnos.brainfuck.programm.command.Command;
import com.kirnos.brainfuck.programm.command.impl.DecrementMemoryValueCommand;
import com.kirnos.brainfuck.programm.command.impl.DecrementMemoryValuePointCommand;
import com.kirnos.brainfuck.programm.command.impl.IncrementMemoryValueCommand;
import com.kirnos.brainfuck.programm.command.impl.IncrementMemoryValuePointCommand;
import com.kirnos.brainfuck.programm.command.impl.InputCommand;
import com.kirnos.brainfuck.programm.command.impl.OutputCommand;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import static com.kirnos.brainfuck.util.Constants.DECREMENT;
import static com.kirnos.brainfuck.util.Constants.INCREMENT;
import static com.kirnos.brainfuck.util.Constants.INPUT;
import static com.kirnos.brainfuck.util.Constants.NEXT;
import static com.kirnos.brainfuck.util.Constants.OUTPUT;
import static com.kirnos.brainfuck.util.Constants.PREVIOUS;

/**
 * Holder for command suppliers for commands which are not composite.
 *
 * @author Serhii Kirnos
 */
public class SupplierCommandContainer {

    public static final Map<String, Supplier<Command>> commandSuppliersMap = new HashMap<>();

    static {
        commandSuppliersMap.put(NEXT, IncrementMemoryValuePointCommand::new);
        commandSuppliersMap.put(PREVIOUS, DecrementMemoryValuePointCommand::new);
        commandSuppliersMap.put(INCREMENT, IncrementMemoryValueCommand::new);
        commandSuppliersMap.put(DECREMENT, DecrementMemoryValueCommand::new);
        commandSuppliersMap.put(INPUT, InputCommand::new);
        commandSuppliersMap.put(OUTPUT, OutputCommand::new);
    }

    /**
     * Returns command supplier object by the given key.
     *
     * @param key Key of the command supplier.
     * @return Command supplier.
     */
    public static Supplier<Command> get(String key) {
        return commandSuppliersMap.get(key);
    }
}
