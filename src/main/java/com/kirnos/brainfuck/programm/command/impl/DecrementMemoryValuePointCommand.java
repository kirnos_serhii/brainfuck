package com.kirnos.brainfuck.programm.command.impl;

import com.kirnos.brainfuck.entity.MachineData;
import com.kirnos.brainfuck.programm.command.Command;

/**
 * Implementation decrement memory pointer command.
 *
 * @author Serhii Kirnos
 */
public final class DecrementMemoryValuePointCommand implements Command {

    @Override
    public void execute(MachineData machineData) {
        machineData.getMemory().decrementDataPointer();
    }
}
