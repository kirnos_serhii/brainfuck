package com.kirnos.brainfuck.programm.command.impl;

import com.kirnos.brainfuck.entity.MachineData;
import com.kirnos.brainfuck.programm.command.Command;

/**
 * Implementation decrement memory command.
 *
 * @author Serhii Kirnos
 */
public final class DecrementMemoryValueCommand implements Command {

    @Override
    public void execute(MachineData machineData) {
        machineData.getMemory().decrementData();
    }
}
