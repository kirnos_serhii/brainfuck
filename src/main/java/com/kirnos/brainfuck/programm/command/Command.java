package com.kirnos.brainfuck.programm.command;

import com.kirnos.brainfuck.entity.MachineData;
import com.kirnos.brainfuck.exception.ExecuteProgramException;

/**
 * An interface for executing Brainfuck language command and building
 * inner structure of the {@link com.kirnos.brainfuck.programm.Program} entity.
 *
 * @author Serhii Kirnos
 */
public interface Command {

    /**
     * Execution method for command.
     *
     * @param machineData machine data for performing command
     * @throws ExecuteProgramException throw exception if there is no possibility to perform command.
     */
    void execute(MachineData machineData) throws ExecuteProgramException;
}
