package com.kirnos.brainfuck.programm.command.impl;

import com.kirnos.brainfuck.entity.MachineData;
import com.kirnos.brainfuck.programm.command.Command;

/**
 * Implementation increment memory command.
 *
 * @author Serhii Kirnos
 */
public final class IncrementMemoryValueCommand implements Command {

    @Override
    public void execute(MachineData machineData) {
        machineData.getMemory().incrementData();
    }
}
