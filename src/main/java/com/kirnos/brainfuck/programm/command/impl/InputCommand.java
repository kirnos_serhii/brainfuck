package com.kirnos.brainfuck.programm.command.impl;

import com.kirnos.brainfuck.entity.MachineData;
import com.kirnos.brainfuck.exception.ExecuteProgramException;
import com.kirnos.brainfuck.programm.command.Command;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class InputCommand implements Command {

    public static final Logger logger = LoggerFactory.getLogger(OutputCommand.class);

    @Override
    public void execute(MachineData machineData) throws ExecuteProgramException {
        logger.error("The input command doesn't supported.");
        throw new UnsupportedOperationException("The input command doesn't supported.");
    }
}
