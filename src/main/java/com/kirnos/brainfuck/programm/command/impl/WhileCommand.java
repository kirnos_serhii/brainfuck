package com.kirnos.brainfuck.programm.command.impl;

import com.kirnos.brainfuck.entity.MachineData;
import com.kirnos.brainfuck.exception.ExecuteProgramException;
import com.kirnos.brainfuck.programm.command.Command;

import java.util.List;

import static com.kirnos.brainfuck.util.Constants.ZERO_CHARACTER;

/**
 * Loop command implementation.
 * This command is a composite, holds a list of internal commands
 * that can be executed repeatedly.
 *
 * @author Serhii Kirnos
 */
public final class WhileCommand implements Command {

    /**
     * List of internal loop commands.
     */
    private List<Command> commandList;

    public WhileCommand(List<Command> commandListParam) {
        this.commandList = commandListParam;
    }

    @Override
    public void execute(MachineData machineData) throws ExecuteProgramException {
        while (!machineData.getMemory().getCurrent().equals(ZERO_CHARACTER)) {
            for (Command command : commandList) {
                command.execute(machineData);
            }
        }
    }
}
