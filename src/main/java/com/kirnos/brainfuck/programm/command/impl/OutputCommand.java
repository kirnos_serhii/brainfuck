package com.kirnos.brainfuck.programm.command.impl;

import com.kirnos.brainfuck.entity.MachineData;
import com.kirnos.brainfuck.exception.ExecuteProgramException;
import com.kirnos.brainfuck.programm.command.Command;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Implementation output data from memory to stream command.
 *
 * @author Serhii Kirnos
 */
public final class OutputCommand implements Command {

    public static final Logger logger = LoggerFactory.getLogger(OutputCommand.class);

    @Override
    public void execute(MachineData machineData) throws ExecuteProgramException {
        try {
            char[] cbuf = new char[]{machineData.getMemory().getCurrent()};
            machineData.getOutputStream().write(cbuf);
            machineData.getOutputStream().flush();
        } catch (IOException e) {
            logger.error("Out in stream error.");
            throw new ExecuteProgramException("Out in stream error.");
        }
    }
}
