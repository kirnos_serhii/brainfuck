package com.kirnos.brainfuck.programm;

import com.kirnos.brainfuck.entity.ByteMemory;
import com.kirnos.brainfuck.entity.MachineData;
import com.kirnos.brainfuck.exception.ExecuteProgramException;
import com.kirnos.brainfuck.programm.command.Command;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * A class to represent the compiled program as a command graph.
 * Provides an interface for running a program on an input data structure.
 *
 * @author Serhii Kirnos
 */
public class Program {

    public static final Logger logger = LoggerFactory.getLogger(Program.class);

    private List<Command> commandList;

    /**
     * Constructor for creating the {@link com.kirnos.brainfuck.programm.Program} entity
     * based on list of commands.
     *
     * @param commandsParam list of command.
     */
    public Program(List<Command> commandsParam) {
        this.commandList = commandsParam;
    }

    /**
     * Method to run the program.
     *
     * @param machineData machine data
     * @throws ExecuteProgramException throws an exception if some command cannot be executed
     */
    public void execute(MachineData machineData) throws ExecuteProgramException {
        for (Command command : commandList) {
            command.execute(machineData);
        }
    }

    /**
     * Helper method for executing a program with a simplified interface.
     *
     * @return program output as a String
     */
    public String execute() {
        String result = "";
        try (ByteArrayOutputStream stream = new ByteArrayOutputStream()) {
            MachineData machineDataParam = new MachineData(new ByteMemory(), null, stream);
            execute(machineDataParam);
            result = new String(stream.toByteArray());
        } catch (IOException | ExecuteProgramException e) {
            logger.error(e.getMessage());
        }
        return result;
    }
}
