package com.kirnos.brainfuck.entity;

/**
 * An interface for representing and manipulating an machine memory.
 *
 * @param <T> Stored information parameter type
 * @author Serhii Kirnos
 */
public interface Memory<T> {

    /**
     * Method for getting the object the pointer points to.
     *
     * @return the object the pointer points to.
     */
    T getCurrent();

    /**
     * Increments the object the pointer points to.
     */
    void incrementData();

    /**
     * Decrements the object the pointer points to.
     */
    void decrementData();

    /**
     * Increments pointer.
     */
    void incrementDataPointer();

    /**
     * Decrements pointer.
     */
    void decrementDataPointer();
}
