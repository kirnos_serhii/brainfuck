package com.kirnos.brainfuck.entity;

/**
 * Class that implements Memory interface.
 * As a storage structure used the array of bytes.
 *
 * @author Serhii Kirnos
 */
public class ByteMemory implements Memory<Character> {

    public static final Integer DEFAULT_MEMORY_SIZE = 30000;

    private byte[] data;

    private int index;

    private final int size;

    @Override
    public Character getCurrent() {
        return (char) (data[index] & 0xFF);
    }

    @Override
    public void incrementData() {
        data[index]++;
    }

    @Override
    public void decrementData() {
        data[index]--;
    }

    @Override
    public void incrementDataPointer() {
        index++;
        if (index >= size) {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public void decrementDataPointer() {
        index--;
        if (index < 0) {
            throw new IndexOutOfBoundsException();
        }
    }

    public ByteMemory() {
        this(DEFAULT_MEMORY_SIZE);
    }

    public ByteMemory(Integer memorySize) {
        size = memorySize;
        data = new byte[size];
    }
}
