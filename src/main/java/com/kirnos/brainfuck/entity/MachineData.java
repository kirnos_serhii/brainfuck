package com.kirnos.brainfuck.entity;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

/**
 * Class for describing machine data structure and input output streams.
 *
 * @author Serhii Kirnos
 */
public class MachineData {

    private Memory<Character> memory;

    private InputStream inputStream;

    private OutputStreamWriter outputStream;

    public MachineData(
            Memory<Character> memoryParam,
            InputStream inputStreamParam,
            OutputStream outputStreamParam) {
        this.memory = memoryParam;
        this.inputStream = inputStreamParam;
        this.outputStream = new OutputStreamWriter(outputStreamParam);
    }

    public Memory<Character> getMemory() {
        return memory;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public OutputStreamWriter getOutputStream() {
        return outputStream;
    }
}
