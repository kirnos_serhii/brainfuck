package com.kirnos.brainfuck;

import com.kirnos.brainfuck.exception.CompileProgramException;
import com.kirnos.brainfuck.programm.Program;
import com.kirnos.brainfuck.programm.compiller.Compiler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

import static com.kirnos.brainfuck.util.Constants.READER;

/**
 * Class for demonstrating the functionality and interface of the Brainfuck
 * language implementation. Uses the console as a command input source.
 * Executes program using default MachineData entity.
 */
public class Application {

    public static final Logger logger = LoggerFactory.getLogger(Compiler.class);

    public static void main(String[] args) {
        System.out.println("Please, input program code: ");
        String programCode;
        try {
            programCode = READER.readLine();
        } catch (IOException e) {
            logger.error("Inability to read data from console.");
            return;
        }

        Compiler compiler = new Compiler();
        Program program;
        try {
            program = compiler.compile(programCode);
        } catch (CompileProgramException e) {
            logger.error("Compile Error.");
            logger.error(e.getLocalizedMessage());
            return;
        }

        String result = program.execute();

        System.out.println("Output: " + result);
    }
}
