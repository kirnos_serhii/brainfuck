package com.kirnos.brainfuck.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Class with application constants.
 *
 * @author Serhii Kintos
 */
public class Constants {

    public static final String OPEN_BRACKET = "[";

    public static final String CLOSE_BRACKET = "]";

    public static final String NEXT = ">";

    public static final String PREVIOUS = "<";

    public static final String INCREMENT = "+";

    public static final String DECREMENT = "-";

    public static final String OUTPUT = ".";

    public static final String INPUT = ",";

    public static final Character ZERO_CHARACTER = 0;

    public static final Set<String> ALL_COMMANDS;

    public static final BufferedReader READER =
            new BufferedReader(new InputStreamReader(System.in));

    static {
        Set<String> set = new HashSet<>();
        set.add(OPEN_BRACKET);
        set.add(CLOSE_BRACKET);
        set.add(NEXT);
        set.add(PREVIOUS);
        set.add(INCREMENT);
        set.add(DECREMENT);
        set.add(OUTPUT);
        set.add(INPUT);
        ALL_COMMANDS = Collections.unmodifiableSet(set);
    }
}
