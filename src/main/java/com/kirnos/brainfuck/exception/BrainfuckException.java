package com.kirnos.brainfuck.exception;

/**
 * Main program exception, all exceptions in Brainfuck should extends this class.
 */
public class BrainfuckException extends Exception {

    public BrainfuckException() {
        super();
    }

    public BrainfuckException(final String message) {
        super(message);
    }
}
