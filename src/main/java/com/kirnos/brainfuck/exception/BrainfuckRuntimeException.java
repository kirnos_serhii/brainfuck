package com.kirnos.brainfuck.exception;

public class BrainfuckRuntimeException extends RuntimeException {

    public BrainfuckRuntimeException() {
        super();
    }

    public BrainfuckRuntimeException(final String message) {
        super(message);
    }
}