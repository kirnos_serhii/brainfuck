package com.kirnos.brainfuck.exception;

/**
 * Runtime Brainfuck program exception.
 */
public class ExecuteProgramException extends BrainfuckException {

    public ExecuteProgramException() {
        super();
    }

    public ExecuteProgramException(final String message) {
        super(message);
    }
}
