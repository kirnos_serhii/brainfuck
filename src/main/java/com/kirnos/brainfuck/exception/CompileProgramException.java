package com.kirnos.brainfuck.exception;

/**
 * Compile Brainfuck program exception.
 */
public class CompileProgramException extends BrainfuckException {

    public CompileProgramException() {
        super();
    }

    public CompileProgramException(final String message) {
        super(message);
    }
}
