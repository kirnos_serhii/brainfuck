package com.kirnos.brainfuck.entity;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ByteMemoryTest {

    public static final Integer MAX_DATA = 255;

    @Test
    public void shouldGetCorrectInitValue() {
        Character expected = 0;
        ByteMemory byteMemory = new ByteMemory(1);

        Character actual = byteMemory.getCurrent();

        assertEquals(expected, actual);
    }

    @Test
    public void shouldCorrectIncrementValue() {
        Character expected = 0;
        ByteMemory byteMemory = new ByteMemory(1);
        do {
            Character actual = byteMemory.getCurrent();
            assertEquals(expected, actual);
            expected++;
            byteMemory.incrementData();
        }
        while (expected <= MAX_DATA);
    }

    @Test
    public void shouldCorrectLoopingValuesWhenIncrementing() {
        Character expected = 0;
        ByteMemory byteMemory = new ByteMemory(1);

        for (int i = 0; i <= MAX_DATA; i++) {
            byteMemory.incrementData();
        }
        Character actual = byteMemory.getCurrent();

        assertEquals(expected, actual);
    }

    @Test
    public void shouldCorrectDecrementValue() {
        int expected = MAX_DATA;
        ByteMemory byteMemory = new ByteMemory(1);
        do {
            byteMemory.decrementData();
            Character actual = byteMemory.getCurrent();

            assertEquals(new Character((char) expected), actual);
            expected--;
        }
        while (expected > 0);
    }

    @Test
    public void shouldThrowIndexOutOfBoundsExceptionWhenIncrement() {
        ByteMemory byteMemory = new ByteMemory(1);
        assertThrows(IndexOutOfBoundsException.class,
                byteMemory::incrementDataPointer);
    }

    @Test
    public void shouldThrowIndexOutOfBoundsExceptionWhenDecrement() {
        ByteMemory byteMemory = new ByteMemory(1);
        assertThrows(IndexOutOfBoundsException.class,
                byteMemory::decrementDataPointer);
    }
}
