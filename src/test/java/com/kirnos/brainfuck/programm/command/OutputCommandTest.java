package com.kirnos.brainfuck.programm.command;

import com.kirnos.brainfuck.entity.MachineData;
import com.kirnos.brainfuck.entity.Memory;
import com.kirnos.brainfuck.exception.ExecuteProgramException;
import com.kirnos.brainfuck.programm.command.impl.OutputCommand;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class OutputCommandTest {

    public static final char ACTUAL_SYMBOL = '0';

    private MachineData machineData;

    private ByteArrayOutputStream outputStream;

    @BeforeEach
    public void setup() {
        Memory<Character> memoryMock = Mockito.mock(Memory.class);
        InputStream inputStreamMock = Mockito.mock(InputStream.class);
        outputStream = new ByteArrayOutputStream();
        machineData = new MachineData(memoryMock, inputStreamMock, outputStream);
        when(memoryMock.getCurrent()).thenReturn(ACTUAL_SYMBOL);
    }

    @Test
    public void shouldOutCorrectDataInStream() throws ExecuteProgramException {
        Command command = new OutputCommand();

        command.execute(machineData);

        assertEquals(new String(outputStream.toByteArray()), Character.toString(ACTUAL_SYMBOL));
    }
}