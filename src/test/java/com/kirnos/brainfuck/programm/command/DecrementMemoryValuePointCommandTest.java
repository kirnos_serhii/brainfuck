package com.kirnos.brainfuck.programm.command;

import com.kirnos.brainfuck.entity.MachineData;
import com.kirnos.brainfuck.entity.Memory;
import com.kirnos.brainfuck.exception.ExecuteProgramException;
import com.kirnos.brainfuck.programm.command.impl.DecrementMemoryValuePointCommand;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.InputStream;
import java.io.OutputStream;

import static org.mockito.Mockito.verify;

class DecrementMemoryValuePointCommandTest {

    private MachineData machineData;

    private Memory<Character> memoryMock;

    @BeforeEach
    public void setup() {
        memoryMock = Mockito.mock(Memory.class);
        InputStream inputStreamMock = Mockito.mock(InputStream.class);
        OutputStream outputStreamMock = Mockito.mock(OutputStream.class);
        machineData = new MachineData(memoryMock, inputStreamMock, outputStreamMock);
    }

    @Test
    public void shouldCallDecrementPointerMethod() throws ExecuteProgramException {
        Command command = new DecrementMemoryValuePointCommand();

        command.execute(machineData);

        verify(memoryMock).decrementDataPointer();
    }
}