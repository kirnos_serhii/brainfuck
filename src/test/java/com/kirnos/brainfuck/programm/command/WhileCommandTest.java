package com.kirnos.brainfuck.programm.command;

import com.kirnos.brainfuck.entity.MachineData;
import com.kirnos.brainfuck.entity.Memory;
import com.kirnos.brainfuck.exception.ExecuteProgramException;
import com.kirnos.brainfuck.programm.command.impl.WhileCommand;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static com.kirnos.brainfuck.util.Constants.ZERO_CHARACTER;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

final class WhileCommandTest {

    public static final Integer NUMBER_COMMANDS = 3;
    public static final Character NOT_NULL_CHARACTER = 'd';

    private MachineData machineData;

    private Memory<Character> memoryMock;

    @BeforeEach
    public void setup() {
        memoryMock = Mockito.mock(Memory.class);
        machineData = Mockito.mock(MachineData.class);
        when(machineData.getMemory()).thenReturn(memoryMock);
    }

    @Test
    public void shouldExecuteAllCommandOnce() throws ExecuteProgramException {
        List<Command> commands = getMockCommands(NUMBER_COMMANDS);
        WhileCommand whileCommand = new WhileCommand(commands);
        when(memoryMock.getCurrent()).thenReturn(NOT_NULL_CHARACTER).thenReturn(ZERO_CHARACTER);

        whileCommand.execute(machineData);

        for (Command command : commands) {
            verify(command, times(1)).execute(machineData);
        }
    }

    @Test
    public void shouldNotExecuteCommands() throws ExecuteProgramException {
        List<Command> commands = getMockCommands(NUMBER_COMMANDS);
        WhileCommand whileCommand = new WhileCommand(commands);
        when(memoryMock.getCurrent()).thenReturn(ZERO_CHARACTER).thenReturn(NOT_NULL_CHARACTER);

        whileCommand.execute(machineData);

        for (Command command : commands) {
            verify(command, times(0)).execute(machineData);
        }
    }

    @Test
    public void shouldThrowExceptionIfCommandThrowException() throws ExecuteProgramException {
        Command command = Mockito.mock(Command.class);
        doThrow(new ExecuteProgramException()).when(command).execute(any());
        List<Command> commands = new ArrayList<>();
        commands.add(command);
        WhileCommand whileCommand = new WhileCommand(commands);
        when(memoryMock.getCurrent()).thenReturn(NOT_NULL_CHARACTER);

        assertThrows(ExecuteProgramException.class, () ->
                whileCommand.execute(machineData));
    }

    private List<Command> getMockCommands(Integer numberCommands) {
        List<Command> commands = new ArrayList<>();
        for (int i = 0; i < numberCommands; i++) {
            Command command = Mockito.mock(Command.class);
            commands.add(command);
        }
        return commands;
    }
}
