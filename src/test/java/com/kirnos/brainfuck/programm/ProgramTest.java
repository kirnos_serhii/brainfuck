package com.kirnos.brainfuck.programm;

import com.kirnos.brainfuck.exception.CompileProgramException;
import com.kirnos.brainfuck.programm.compiller.Compiler;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ProgramTest {

    @Test
    public void ShouldCorrectExecuteProgramIntegrationTest() throws CompileProgramException {
        String expectedString = "Hello World!\n";
        String correctProgramHelloWorld = "++++++++++[>+++++++>++++++++++>+++>+<<<<-]>++.>+.+++++++..+++.>++.<<+++++++++++++++.>.+++.------.--------.>+.>.";
        Compiler compiler = new Compiler();

        Program program = compiler.compile(correctProgramHelloWorld);
        String actualString = program.execute();

        assertEquals(expectedString, actualString);
    }

}