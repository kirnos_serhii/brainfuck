package com.kirnos.brainfuck.programm.compiller;

import com.kirnos.brainfuck.exception.CompileProgramException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CompilerTest {

    private Compiler compiler;

    private Method findEndCycleMethod;

    @BeforeEach
    public void setup() throws NoSuchMethodException {
        compiler = new Compiler();
        findEndCycleMethod = Compiler.class.getDeclaredMethod("findEndCycle", String[].class, Integer.class, Integer.class);
        findEndCycleMethod.setAccessible(true);
    }

    @Test
    public void shouldThrowErrorIfIncorrectBrackets() {
        String programIncorrectBrackets = ">>[>.[]";

        assertThrows(CompileProgramException.class, () ->
                compiler.compile(programIncorrectBrackets));
    }

    @Test
    public void shouldThrowErrorIfIncorrectProgramSymbols() {
        String programIncorrectSymbols = ">>[>.[]].>>+/>.";

        assertThrows(CompileProgramException.class, () ->
                compiler.compile(programIncorrectSymbols));
    }

    @Test
    public void shouldDoesNotThrowErrorIfProgramIsCorrect() {
        String correctProgram = ">>[>.[]].";

        assertDoesNotThrow(() ->
                compiler.compile(correctProgram));
    }

    @Test
    public void shouldReturnCorrectBucketIndexIfBucketEnd() throws InvocationTargetException, IllegalAccessException {
        Integer expectedValue = 6;
        String programIncorrectBrackets = ">>[>>>]";

        Integer actualValue = (Integer) findEndCycleMethod.invoke(compiler, programIncorrectBrackets.split(""), 3, 6);

        assertEquals(expectedValue, actualValue);
    }

    @Test
    public void shouldReturnCorrectBucketIndexIfBucketMiddle() throws InvocationTargetException, IllegalAccessException {
        Integer expectedValue = 6;
        String programIncorrectBrackets = ">>[>>>].<";

        Integer actualValue = (Integer) findEndCycleMethod.invoke(compiler, programIncorrectBrackets.split(""), 3, 8);

        assertEquals(expectedValue, actualValue);
    }

    @Test
    public void shouldReturnCorrectBucketIndexIfBucketStart() throws InvocationTargetException, IllegalAccessException {
        Integer expectedValue = 3;
        String programIncorrectBrackets = ">>[]>><+++";

        Integer actualValue = (Integer) findEndCycleMethod.invoke(compiler, programIncorrectBrackets.split(""), 3, 8);

        assertEquals(expectedValue, actualValue);
    }
}